//
//  ViewController.swift
//  unitconventor
//
//  Created by GDD Student on 9/5/16.
//  Copyright © 2016 OMTA. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate {

    let userDefaultsLastRowKey = "defaultCelsiusPickerRow"
    @IBOutlet weak var celsiusPicker: UIPickerView!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet var temperatureRange: TemperatureRange!
    private let converter = UnitConverter()
    
    func initialPickerRow() -> Int{
    
        let savedRow =  NSUserDefaults.standardUserDefaults().objectForKey(userDefaultsLastRowKey)as? Int
        if let row = savedRow{
        return row
        }
        else{
            return celsiusPicker.numberOfRowsInComponent(0) / 2
        
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //let lowerBound = -100
       // let upperBound = 100
        // for var index = lowerBound;index <= upperBound; ++index{
       let row = initialPickerRow()
        celsiusPicker.selectRow(row, inComponent: 0, animated: false)
        pickerView(celsiusPicker, didSelectRow: row, inComponent: 0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int ) -> String?
    {
        let celciusValue = temperatureRange.values[row]
        return "\(celciusValue) ℃ "
    }
     func saveSelectedRow(row:Int){
        
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setInteger(row, forKey: "defaultCelsiusPickerRow")
        defaults.synchronize()
        

    }
    //save the last chosen temoerature
    func displayConvertedTemperatureForRow(row:Int){
        let degreesCelsius = temperatureRange.values[row]
        temperatureLabel.text = "\(converter.degreesFahrenheit(degreesCelsius))°F"
    }
    
       
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int , inComponent component: Int ){
    
           /*let degreesCelsius = temperatureRange.values[row]
            temperatureLabel.text = "\(converter.degreesFahrenheit(degreesCelsius))°F"
        */
            displayConvertedTemperatureForRow(row)
            saveSelectedRow(row)
        
                   
    }
}

